'use strict';
import jsonwebtoken from "jsonwebtoken";
export default (body, secret, cb) => {
	if (!body) {
		return cb(new Error('invalid jwtdata'));
	}

	jsonwebtoken.verify(body.toString('utf8'), secret, {
		algorithm: 'HS256'
	}, cb);
};