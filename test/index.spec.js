 import app from "../server/app";
import request from "supertest"

 describe('POST /save', ()=>{
    test('should respond with a 200 status code', async ()=>{
        const response = await request(app).post('/save').send()
        expect(response.statusCode).toBe(200);

    })
 })


 describe('POST /validate', ()=>{
    test('should respond with a 200 status code', async ()=>{
        const response = await request(app).post('/save').send()
        expect(response.statusCode).toBe(200);

    })
 })

 describe('POST /publish', ()=>{
    test('should respond with a 200 status code', async ()=>{
        const response = await request(app).post('/save').send()
        expect(response.statusCode).toBe(200);

    })
 })



 describe('get /', ()=>{
    test('should respond with a content type text/html; charset=UTF-8', async ()=>{
        const response = await request(app).get('/').send()
        expect(response.headers['content-type']).toEqual(expect.stringContaining("text/html"));
    })
 })

 describe('get /config.json', ()=>{
    test('should respond with a content type application/json', async ()=>{
        const response = await request(app).get('/config.json').send()
        expect(response.body.configurationArguments).toBeDefined()
    })
    test('should respond with a content type application/json', async ()=>{
        const response = await request(app).get('/config.json').send()
        expect(response.body.type).toBe("REST")
    })
 })



 describe('post /execute', ()=>{
    test('should respond unauthorized when request without body', async ()=>{
        const response = await request(app).post('/execute').send()
        expect(response.statusCode).toBe(401);
    })
    
 })